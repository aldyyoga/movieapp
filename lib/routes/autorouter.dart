import 'package:moveeapp/module/features/moviecrud/screens/moviecrud.dart';
import 'package:moveeapp/module/features/movielist/screens/movielist.dart';
import 'package:auto_route/auto_route.dart';


@MaterialAutoRouter(
    replaceInRouteName: 'Page,Route',
    routes: <AutoRoute>[
      AutoRoute(name: 'MovieListRouter', page: MovieList, initial: true),
      AutoRoute(name: 'MovieCrudRouter',page: MovieCrud),
      
    ],
)
class $AppRouter {}