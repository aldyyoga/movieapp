// To parse this JSON data, do
//
//     final movieModel = movieModelFromMap(jsonString);

import 'dart:convert';

class MovieModel {
  MovieModel({
    required this.id,
    required this.title,
    required this.director,
    required this.summary,
    required this.tags,
  });

  final int id;
  final String title;
  final String director;
  final String summary;
  final Tags tags;

  MovieModel copyWith({
    int? id,
    required String title,
    required String director,
    required String summary,
    required Tags tags,
  }) =>
      MovieModel(
        id: id ?? this.id,
        title: title ?? this.title,
        director: director ?? this.director,
        summary: summary ?? this.summary,
        tags: tags ?? this.tags,
      );

  factory MovieModel.fromJson(String str) =>
      MovieModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory MovieModel.fromMap(Map<String, dynamic> json) => MovieModel(
        id: json["id"] == null ? null : json["id"],
        title: json["title"] == null ? null : json["title"],
        director: json["director"] == null ? null : json["director"],
        summary: json["summary"] == null ? null : json["summary"],
        tags: json["tags"] == null ? null : Tags.fromMap(json["tags"]),
      );

  Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "title": title == null ? null : title,
        "director": director == null ? null : director,
        "summary": summary == null ? null : summary,
        "tags": tags == null ? null : tags.toMap(),
      };
}

class Tags {
  Tags({
    required this.idTags,
    required this.tagsTitle,
  });

  final int idTags;
  final String tagsTitle;

  Tags copyWith({
    required int idTags,
    required String tagsTitle,
  }) =>
      Tags(
        idTags: idTags ?? this.idTags,
        tagsTitle: tagsTitle ?? this.tagsTitle,
      );

  factory Tags.fromJson(String str) => Tags.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Tags.fromMap(Map<String, dynamic> json) => Tags(
        idTags: json["idTags"] == null ? null : json["idTags"],
        tagsTitle: json["tagsTitle"] == null ? null : json["tagsTitle"],
      );

  Map<String, dynamic> toMap() => {
        "idTags": idTags == null ? null : idTags,
        "tagsTitle": tagsTitle == null ? null : tagsTitle,
      };
}
