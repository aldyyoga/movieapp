import 'dart:math';

import 'package:moveeapp/module/MovieHome/data/model/movie.dart';

// import 'model/movie.dart';

abstract class MovieRepository {
  Future<MovieModel> fetchMovie(String title, String director, String summary);
}

class DummyMovieRepository implements MovieRepository {
  // double cachedTempCelsius;

  @override
  Future<MovieModel> fetchMovie(String title, String director, String summary) {
    // Simulate network delay
    return Future.delayed(
      Duration(seconds: 1),
      () {
        final random = Random();

        // Simulate some network error
        if (random.nextBool()) {
          throw NetworkError();
        }

      
        // Return "fetched" weather
        return MovieModel(
            // cityName: cityName,
            // Temperature between 20 and 35.99
            // temperatureCelsius: cachedTempCelsius,
            );
      },
    );
  }
}

class NetworkError extends Error {}
