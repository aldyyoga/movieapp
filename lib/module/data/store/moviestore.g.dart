// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'moviestore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MovieStore on _MovieStore, Store {
  final _$movieModelAtom = Atom(name: '_MovieStore.movieModel');

  @override
  MovieModel get movieModel {
    _$movieModelAtom.reportRead();
    return super.movieModel;
  }

  @override
  set movieModel(MovieModel value) {
    _$movieModelAtom.reportWrite(value, super.movieModel, () {
      super.movieModel = value;
    });
  }

  final _$errorMessageAtom = Atom(name: '_MovieStore.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.reportRead();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.reportWrite(value, super.errorMessage, () {
      super.errorMessage = value;
    });
  }

  final _$_MovieStoreActionController = ActionController(name: '_MovieStore');

  @override
  void addMovie(String title) {
    final _$actionInfo =
        _$_MovieStoreActionController.startAction(name: '_MovieStore.addMovie');
    try {
      return super.addMovie(title);
    } finally {
      _$_MovieStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
movieModel: ${movieModel},
errorMessage: ${errorMessage}
    ''';
  }
}
