import 'package:mobx/mobx.dart';
import 'package:moveeapp/module/data/model/movie.dart';
import 'package:moveeapp/module/data/repository/movierepository.dart';

part 'moviestore.g.dart';

class MovieStore extends _MovieStore with _$MovieStore {
  MovieStore(MovieRepository movieRepository) : super(movieRepository);
}

abstract class _MovieStore with Store {
  final MovieRepository _movieRepository;

  _MovieStore(this._movieRepository);

  @observable
  MovieModel movieModel;

  @observable
  String errorMessage;

  @action
  void addMovie(String title) {
    final todo = MovieModel(title: title);
    // todos.add(todo);
  }
}
